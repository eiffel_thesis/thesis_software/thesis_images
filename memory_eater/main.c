#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>


/**
 * Allocate as many bytes as given as argument and wait for a signal to arrive.
 * @param argc The number of argument of the program, it must be two.
 * @param argv argv[0] is the program name while argv[1] should be a number of
 * bytes.
 * @return EXIT_SUCCESS if everything went smooth, EXIT_FAILURE otherwise.
 */
int main(int argc, char** argv){
	int ret;

	unsigned long long int size;
	void *buffer;

	sigset_t set;

	ret = EXIT_SUCCESS;

	if(argc != 2){
		fprintf(stderr, "Usage: %s memory_bytes\n", argv[0]);

		return EXIT_FAILURE;
	}

	// Set errno to 0 as advised in strtoull man page.
	errno = 0;

	size = strtoull(argv[1], NULL, 10);

	if(errno){
		perror("strtoull");

		return EXIT_FAILURE;
	}

	if(posix_memalign(&buffer, getpagesize(), size)){
		perror("posix_memalign");

		return EXIT_FAILURE;
	}

	// Write whole buffer with 0s to bypass lazy allocation.
	if(!memset(buffer, 0, size)){
		fprintf(stderr, "Problem while writing buffer!\n");

		ret = EXIT_FAILURE;

		goto release;
	}

	if(sigemptyset(&set) == -1){
		fprintf(stderr, "Problem while emptying set!\n");

		ret = EXIT_FAILURE;

		goto release;
	}

	if(sigsuspend(&set) != -1){
		perror("sigsuspend");

		ret = EXIT_FAILURE;
	}

release:
	free(buffer);

	return ret;
}
