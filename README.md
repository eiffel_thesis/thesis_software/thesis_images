# Collection of `docker` images used during experiments

This repository contains a collection of `docker` images used during my experiments.
The main image is `sysbench`.

## Probe

The file `sysbench/probe.pl` is used in experiment so containers can indicate
their performance to the kernel.
Basically, this file will compared the sysbench output to a given as argument number considered as the reference performance.

There are multiples cases:
- If `sysbench` transactions rate is limited, there are 3 cases:

	1. If there is no transactions in queue, the container has the best possible performance.
	1. If the average of answered transactions is greater or equal than the reference performance the container has satisfying performance.
	1. Otherwise, the container has bad performance.

- Otherwise, _i.e._ if the transaction rate is unlimited, there are only 2 cases:
	1. Either the average of answered transactions is greater or equal than the reference performance so container has good performance.
	1. Or the container has bad performance.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

I am only the author of the `Dockerfile` and some Perl scripts.

## License

This project is licensed under the MPL 2.0 License.