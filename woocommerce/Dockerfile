# Version 5.7 can seem old since it was released in 2015.
# But it used as default the old way of handling password and the new one seems
# to be a problem with docker.
FROM mysql:5.7

# Since this for experiment only we do not want to bother with password.
ENV MYSQL_ALLOW_EMPTY_PASSWORD="yes"

# WordPress needs MySql to contain a database called wordpress.
# The file schema.sql contains the creation of this database but it cannot been
# execute with mysql in a RUN command because the mysqld will not be launched.
# So docker found a feint and you just need to put your schema file in
# /docker-entrypoint-initdb.d. The files contained here will be executed by
# entrypoint during the build (see https://stackoverflow.com/a/40031181).
ADD schema.sql /docker-entrypoint-initdb.d

# wp-cli needs php and php-mysql to run without problem.
# generate_fake_images.php needs php-gd.
# wp cyclone needs to be taken from git and installed through composer which
# needs php-curl and zip.
# libfile-tail-perl is needed by our probe.
# liblist-moreutils-perl is needed by update_image.pl.
RUN apt-get -qy update && apt-get -qy install php php-mysql php-gd git composer php-curl zip libfile-tail-perl liblist-moreutils-perl

# Remove Apache index.html to replace it by wordpress index.php.
RUN rm /var/www/html/index.html

# Add response time in µs to the default log.
# The default log can be found in /var/log/apache2/access.log.
RUN perl -pi -e 's/(" combined)/ response_time: %D us$1/' /etc/apache2/apache2.conf

# Get wordpress-cli
# ADD is powerful since it can take file from URL!
# And it works compared to 'curl -O' because I had a problem with that...
ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar /usr/local/bin/wp
RUN chmod +x /usr/local/bin/wp

# Change WORKDIR as /var/www/html so WordPress will be installed in this
# directory.
WORKDIR /var/www/html

# db.sql contains an export of Woocommerce tables which contains users, reviews,
# orders and products.
# It takes too much time to generate it at run time with "wp cyclone" so I
# generated it once and export it.
# This file builds a X GB database and was obtained by running:
# wp cyclone seed --allow-root
# wp cyclone products 10000 --allow-root
# wp cyclone orders 15000 --allow-root
# wp cyclone reviews 15000 --allow-root
# This file will be imported during preparation of experiment.
COPY db.sql /var/www/html

# Add the probe.
COPY probe.pl /var/www/html

# Add generate_fake_images script.
ADD generate_fake_images /var/www/html/generate_fake_images

# Add the docker-entrypoint.sh of MySql to docker-entrypoint.d
# All scripts contained in this directory will be run by our custom
# docker-entrypoint.sh
RUN mkdir /docker-entrypoint.d
ADD mysql-entrypoint.sh /docker-entrypoint.d

# Add wp_config.sh to docker-entrypoint.d.
# This script contains everything to install and setup quickly WordPress.
ADD wp_config.sh /docker-entrypoint.d

# Add update_image.pl to docker-entrypoint.d.
# This script is executed by above script to correct the image path in database
# so they are not broken.
ADD update_image.pl /var/www/html

# Add the docker-entrypoint.sh of WordPress to docker-entrypoint.d
ADD wordpress-entrypoint.sh /docker-entrypoint.d

# Mark all those files as executable.
RUN chmod +x /docker-entrypoint.d/*

# Let's go to the next level in writing Dockerfile.
# docker-entrypoint.sh is generally a script used at the beginning of a container
# execution.
# It permits to run some commands to prepare the container.
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

# This argument will be used to set WordPress site URL.
# Basically URL is localhost:argument.
# If multiple containers are launched they should have their own ports.
# The container can be launched with: docker run -d -p8080:80 woocommerce
# Or to change the port with: docker run -d -p8081:80 woocommerce 8081
CMD ["8080"]