#! /usr/bin/php
<?php
/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: MPL-2.0
 */
require __DIR__ . '/vendor/autoload.php';

use bheller\ImagesGenerator\ImagesGeneratorProvider;

/*
 * The images will be stored in this directory.
 * Its name will be completed below.
 */
const PATH_FORMAT = "/var/www/html/wp-content/uploads/%s";

// Image width in pixel.
const WIDTH = 1080;

// Image height in pixel.
const HEIGHT = 720;

// Each PERCENT a message is printed to inform users of script progress.
const PERCENT = 10;

// Translate above PERCENT to fraction.
const FRACTION = 100 / PERCENT;

if($argc != 2)
	die("Usage: ${argv[0]} number_of_pic_to_generate" . PHP_EOL);

// Translate string to int.
$pictures = intval($argv[1]);

// Complete path with "actual_year/actual_month".
$path = sprintf(PATH_FORMAT, date('Y/m'));

// If the directory does not exist we create it.
if(!file_exists($path) && !is_dir($path))
	if(!mkdir($path, 0755, true))
		die("Problem while mkdir ${path}!");

// Initialize faker with factory.
$faker = \Faker\Factory::create();

/*
 * Add new provider: ImageGenerator.
 * It permits generating simple image locally.
 */
$faker->addProvider(new ImagesGeneratorProvider($faker));

echo "This script will generate ${pictures} pictures, please be patient...\n";

for($i = 0; $i < $pictures; $i++){
	$faker->imageGenerator($path, WIDTH, HEIGHT, 'jpg', true, $faker->word, $faker->hexColor, $faker->hexColor);

	# Inform each ten percents.
	# If modulo gives 0 we did PERCENT percents.
	if($i && !($i % ($pictures / FRACTION))){
		$percent = $i / $pictures * 100;

		// Use '\r' (carriage return) to go back to the beginning of line.
		echo "Progress: ${i}/${pictures} (${percent}%)\r";
	}
}

echo "Generation terminated (100%)\n";
?>