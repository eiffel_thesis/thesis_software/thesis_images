#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

# This script will be called at startup of wordpress container after the default
# image docker-entrypoint.sh.
# It will basically run everything so woocommerce is OK.

# Firstly set PORT to $1 and put this in .bashrc file so PORT can be used in
# docker exec through bash.
PORT=$1
echo "export PORT=${PORT}" >> ~/.bashrc

# We will download wordpress.
wp core download --allow-root

# Then we need to configure it.
wp config create --dbname=wordpress --dbuser=root --allow-root --extra-php <<PHP
define( 'CONCATENATE_SCRIPTS', false );
define( 'SCRIPT_DEBUG', true );
PHP

# We then need to create the tables.
# We also need to set site URL to localhost:port.
# So there will not be any problem when acceding it through Firefox with, for
# example localhost:8080.
wp core install --url=localhost:$PORT --title=site --admin_user=wordpress --admin_email=nobody@nobody.org --skip-email --allow-root

# Install "No Login" plugin to avoid login.
wp plugin install no-login --activate --allow-root

# Install "Disable Emails" plugin to avoid sending mail each user creation.
wp plugin install disable-emails --activate --allow-root

# Install "Auto Approve Comments" plugin to automatically approving comments.
wp plugin install auto-approve-comments --activate --allow-root

# Install WooCommerce and activate it.
wp plugin install woocommerce --activate --allow-root

# Get Storefront theme.
wp theme install storefront --activate --allow-root

# Get wc-cyclone from github.
git clone https://github.com/eiffel-fl/wc-cyclone.git

cd wc-cyclone

# Install its dependencies.
composer install

cd ..

# Zip it to be able to install it with wp plugin.
zip -r wc-cyclone.zip wc-cyclone/

# Install it with wp plugin.
wp plugin install wc-cyclone.zip --activate --allow-root

# Install all the pages (cart, checkout, etc.) for WooCommerce.
# Command taken from:
# https://github.com/woocommerce/woocommerce/wiki/WC-CLI-Overview#command-1
wp wc tool run install_pages --allow-root

# Deactivate no-login plugin since we setup everyhing.
# We want now to be able to log in as simple client.
wp plugin deactivate no-login --allow-root

# Reactivate disable-emails plugin because above activation seems to not work.
wp plugin activate disable-emails --allow-root

# Replace default port 8080 by given port.
perl -pi -e "s/localhost:8080/localhost:${PORT}/g" db.sql

# Seed the images.
wp cyclone seed --allow-root

image_dir=wp-content/uploads/$(date +%Y/%m)

# Move the image from cyclone directory to wordpress content directory.
mv wp-content/plugins/wc-cyclone/data/products/images/books/* $image_dir

cd generate_fake_images

# Install home made script generate_fake_images.php dependencies.
composer install

# Generate 30000 fake images.
php generate_fake_images.php 30000

cd ..

# Update the image path in db.sql
perl update_image.pl $image_dir

# Import the database.
wp db import db.sql --allow-root