#!/bin/bash
# General docker-entrypoint.sh taken from:
# https://www.camptocamp.com/actualite/utiliser-scripts-dentrypoint-docker-flexible/

DIR=/docker-entrypoint.d

if [[ -d "$DIR" ]]
then
	# mysqld needs to be started before WordPress can be configurated because
	# wp-cli will write in databases.
	/bin/bash ${DIR}/mysql-entrypoint.sh mysqld

	# Pass port given as argument to configure WordPress site URL.
	/bin/bash ${DIR}/wp_config.sh $1
fi

source /etc/apache2/envvars
/bin/bash ${DIR}/wordpress-entrypoint.sh apache2 -DFOREGROUND