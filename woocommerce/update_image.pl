#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;


my $DB = 'db.sql';
my $content;

my $dirname;
my $dd;

my @images;
my $current_image;

my $fd;

my $matches;
my @replacements;

# Get localtime.
my ($sec, $min, $hour, $mday, $month, $year, $wday, $yday, $isdst) =                                                       localtime(time);

# $year contains the number of years since 1900.
$year += 1900;

# $month are in [0, 11] so we add to pass it in [1, 12].
# sprintf is used to make one digit number become two digits.
$month = sprintf '%02d', $month + 1;

$dirname = shift or die "Usage: $0 image_directory";

# Open the directory whom name is given as argument.
opendir $dd, $dirname or die "Problem while opening $dirname: $!";

# Get all the filenames in the directory.
@images = readdir $dd;

# Remove '.' and '..'.
shift @images;
shift @images;

closedir $dd or warn "Problem while closing $dirname: $!";

open $fd, '<', $DB or die "Problem while opening $DB: $!";

# Read the whole file and store it in content.
# '-s' operator permits to get the file size.
read $fd, $content, -s $fd or die "Problem while reading $DB: $!";

close $fd or warn "Problem while closing $DB: $!";

# Get all the image filename in db.sql and replace them by %s.
$matches = $content =~ s@(_wp_attached_file',')\d+/\d+/[-\w]+?(?:-\d+)?\.jpg@$1%s@g;

$current_image = 0;

for my $i (0 .. $matches - 1){
	my $image;

	# Get an image filename.
	$image = $images[$current_image];

	# Push it in replacements.
	push @replacements, "${year}/${month}/${image}";

	# Pass to next image.
	$current_image++;

	# Normally and thanks to generate_fake_images.php we should not need this if
	# anymore.
	# But it brings security so I keep it.
	if($current_image == scalar @images){
		$current_image = 0;
	}
}

# Before replacing the %s we need to handle some "word" in $content that can be
# interpreted as format.
# The guilty word is %5 like in %5D and %5B.
# We replace it, temporary, with #5.
$content =~ s/%5/#5/g;

# Use replacements to replace the %s.
# This trick is faster than doing a regex for each image.
$content = sprintf $content, @replacements;

# Restore the %5 in $content.
$content =~ s/#5/%5/g;

open $fd, '>', $DB or die "Problem while opening $DB: $!";

print {$fd} $content;

close $fd or warn "Problem while closing $DB: $!";