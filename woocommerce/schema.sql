-- Default WordPress database will be named wordpress.
create database wordpress;
-- Default WordPress user will be named wordpress.
-- We will give all privileges on those database to this user.
create user wordpress;
grant all privileges on wordpress.* to 'wordpress';