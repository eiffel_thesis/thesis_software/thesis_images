#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;
use Time::HiRes qw(setitimer ITIMER_REAL);
use File::Tail;


my $NO_COLOR = 0;
my $RED = 1;
my $ORANGE = 2;
my $GREEN = 3;

my $fd;
my $color_log;

my $time;
my $request_this_second;
my $second;

my $log;

my $expected_response_time;
my $duration;

# If it is impossible to open memory.color this means this file does not exist.
# So we use a vanilla kernel, we will just use the probe to log the color.
open $fd, '>', '/sys/fs/cgroup/memory/memory.color' or open $fd, '>', '/dev/null' or die "Can not open either '/sys/fs/cgroup/memory/memory.color' or '/dev/null': $!";

open $color_log, '>', 'color.log' or die "Can not open color.log: $!";

# File::Tail was designed to read log file while they are being written which is
# our case here.
# We will not wait more than 1 second before checking the log.
$log = File::Tail->new(name => '/var/log/apache2/access.log', maxinterval => 1);

$expected_response_time = shift @ARGV or die "Usage: $0 expected_response_time_in_µs";

# Install a handler for SIGALRM.
$SIG{ALRM} = sub {
	my $mean_time;
	my $color;

	# Do a simple if to avoid dividing by 0.
	if($request_this_second > 0){
		# Compute mean response time for this second.
		$mean_time = $time / $request_this_second;
	}

	# If response time is higher than expected container has bad performance.
	if(defined $mean_time && $mean_time > $expected_response_time){
		$color = $RED;
	}else{
		# If above if is false $mean_time is undefined so no transactions came this
		# second so performance are good.
		# Or $mean_time is defined and below $expected_response_time so performance
		# are good too!
		$color = $GREEN;
	}

	# NOTE We use syswrite because it is not buffered.
	# TODO Test this write can be a good idea...
	syswrite $fd, "${color}\n";

	# Log the time inside the experiment and the color associated.
	print $color_log "${second};${color}\n";

	# Reset variables.
	$time = 0;
	$request_this_second = 0;

	# Progress the time inside the experiment.
	$second++;

	# Fire SIGALRM in one second so this handler is called every second.
	alarm 1;
};

$time = 0;
$request_this_second = 0;
$second = 0;

# Fire SIGALRM in one second.
alarm 1;

# According to source code of File::Tail the read returns the result of substr.
# This result can be undef so this condition can be false so the program will
# exit.
while(defined(my $line = $log->read())){
	# To exit the while we we will send a GET request on /STOP.
	# If this request is parsed by the probe it will quit the loop.
	if($line =~ m-/STOP-){
		last;
	}

	if($line =~ m/response_time: (\d+) us/){
		# Accumulate response time in time and increment request since we deal with
		# one request.
		# These variables will be used by SIGALRM handler.
		$time += $1;
		$request_this_second++;
	}

	print $line;
}

# Reset color at the end of experiment.
syswrite $fd, "${NO_COLOR}\n";

# Close file descriptor.
close $fd or warn "Problem while closing either '/sys/fs/cgroup/memory/memory.color' or '/dev/null': $!";
close $color_log or warn "Problem while closing color.log: $!";

# Empty access log.
# Since we will run the probe N-th time we need to empty the access.log
# otherwise its content will be parsed by the probe when it is run the second
# time for example.
# I do agree that this is not really pro but this is more for an experimental
# finality.
open $fd, '>', '/var/log/apache2/access.log' or die "Can not open access.log: $!";
close $fd or warn "Problem while closing either 'access.log': $!";