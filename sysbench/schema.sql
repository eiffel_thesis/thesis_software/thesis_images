-- By default sysbench manipulates a database called "sbtest". The problem is
-- that sysbench will not create this database so we need to create it before.
create database sbtest;
-- By default sysbench user is sbtest.
-- This user needs to be able to create table, insert records and select them
-- in the sbtest database.
create user sbtest;
grant all privileges on sbtest.* to 'sbtest';
