#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;


my $NO_COLOR = 0;
my $RED = 1;
my $ORANGE = 2;
my $GREEN = 3;

my $DEFAULT_PERIOD = 1;

my $rates;
my $times;

my $fd;
my $fd_period;
my $log;

my $expected_transactions;
my $probe_period;

my $tps_avg;
my $queue_length_avg;
my $probe_cycles;

# If it is impossible to open memory.color this means this file does not exist.
# So we use a vanilla kernel, we will just use the probe to log the color.
open $fd, '>', '/sys/fs/cgroup/memory/memory.color' or open $fd, '>', '/dev/null' or die "Can not open either '/sys/fs/cgroup/memory/memory.color' or '/dev/null': $!";

# On a vanilla kernel, the first open will fail and /dev/null will be used as a replacement.
open $fd_period, '>', '/sys/fs/cgroup/memory/memory.special_reclaim_period' or open $fd_period, '>', '/dev/null' or die "Can not open either '/sys/fs/cgroup/memory/memory.special_reclaim_period' or '/dev/null': $!";

open $log, '>', 'color.log' or die "Can not open color.log: $!";

$expected_transactions = shift @ARGV or die "Usage: $0 expected_transactions [probe_period]";

# This argument is optional to avoid breaking already existing experiments.
$probe_period = shift @ARGV;

# If the command line does not given a value, it is undef.
# We set its default value: the probe informs the kernel each second.
if(!defined $probe_period){
	$probe_period = $DEFAULT_PERIOD;
}

# Set the kernel reclaim period.
# WARNING Even though there is one file per cgroup to read/write this value, it
# is a global one.
# So basically, "last writer wins".
syswrite $fd_period, "${probe_period}\n";

$tps_avg = 0;
$queue_length_avg = 0;
$probe_cycles = 0;

while(<STDIN>){
	if($_ =~ m/\[ (\d+)s \].* tps: (\d+\.\d+) .* rate: (\d+) queue length: (\d+)/){
		my $time;
		my $rate;

		my $color;

		# Get "easy" values.
		$time = $1;
		$rate = $3;

		# Accumulate others values.
		$tps_avg += $2;
		$queue_length_avg += $4;

		# Pass to next cycle.
		$probe_cycles++;

		# If we collect metrics for $probe_period we need to compute average and
		# based on it inform the kernel.
		# If $probe_period == 1, the kernel is informed each second.
		# Otherwise, i.e. if $probe_period = n where n > 1, it is informed each n
		# second.
		if($probe_cycles == $probe_period){
			# Compute mean.
			$tps_avg /= $probe_cycles;
			$queue_length_avg /= $probe_cycles;

			# Take decision based on these means.
			if($rate){ # If rate is limited, we have 3 different cases.
				if(!$queue_length_avg){
					# If the queue is empty it means that the container performs perfectly.
					# This can happen during low activity period (e.g. 200 transactions per
					# second). But it can also happen during high activity period (e.g. 1700
					# transactions) so this means that the container has the best performance
					# possible.
					# The container is so marked as GREEN.
					$color = $GREEN;
				}elsif($tps_avg >= $expected_transactions){
					# If the queue contains transactions but the transactions dealt this
					# second is greater than the SLA this means that the container has good
					# performance.
					# But maybe with more memory it could have deal with the transactions in
					# its queue.
					# For this, we marked it as ORANGE.
					$color = $ORANGE;
				}else{
					# Otherwise it means that the queue contains transactions and the
					# transactions dealt this second is inferior to the SLA.
					# The container has bad performance and should be marked as RED.
					$color = $RED;
				}
			}else{ # Otherwise there are only 2 cases.
				if($tps_avg >= $expected_transactions){
					# If the container dealt with more transactions than the SLA it has good
					# performance so we marked it as GREEN.
					$color = $GREEN;
				}else{
					# Otherwise it dealt with less transactions than the SLA so it has bad
					# performance and we marked it as RED.
					$color = $RED;
				}
			}

			# Reset everything.
			$tps_avg = 0;
			$queue_length_avg = 0;
			$probe_cycles = 0;

			# Inform the kernel.
			# NOTE We use syswrite because it is not buffered.
			syswrite $fd, "${color}\n";

			# Log the time inside the benchmark and the color associated.
			print $log "${time};${color}\n";
		}
	}

	print $_;
}

# Reset color and reclaim period at the end of parsing.
syswrite $fd, "${NO_COLOR}\n";
syswrite $fd_period, "${DEFAULT_PERIOD}\n";

close $fd or warn "Problem while closing either '/sys/fs/cgroup/memory/memory.color' or '/dev/null': $!";
close $log or warn "Problem while closing color.log: $!";