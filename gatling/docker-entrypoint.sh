#!/bin/bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0


# Replace default HTTP port to given argument.
perl -pi -e "s/PORT = 8080/PORT = $1/" /gatling-charts-highcharts-bundle-3.2.1/user-files/simulations/woocommerce/common/HttpProtocol.scala

# Since we are in a container if we try to access localhost it will be container
# localhost and not host localhost.
# We then need to get IP of host localhost with the following command (this was
# taken from https://stackoverflow.com/a/31328031):
ip=$(ip route show | awk '/default/ {print $3}')

# Add IP and port to bashrc so each time we use bash we can get those
# information with those variables.
echo "export IP='${ip}'" >> ~/.bashrc
echo "export PORT='$1'" >> ~/.bashrc

# We now replace default HTTP URL by our ip:
perl -pi -e "s/URL = \"localhost\"/URL = \"$ip\"/" /gatling-charts-highcharts-bundle-3.2.1/user-files/simulations/woocommerce/common/HttpProtocol.scala

# Sleep infinitely and wait for exec.
sleep inf