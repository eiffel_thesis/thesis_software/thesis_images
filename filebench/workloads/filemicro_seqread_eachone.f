#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"%Z%%M%	%I%	%E% SMI"

# Single threaded sequential reads (1MB I/Os) on a 1G file then on a 10G file.

# Global defines.
set $dir=/tmp
set $cached=false
set $iosize=1m
set $nthreads=1

# Specific defines.
set $small_filesize=1g
set $large_filesize=10g

define file name=smallfile,path=$dir,size=$small_filesize,prealloc,reuse,cached=$cached
define file name=largefile,path=$dir,size=$large_filesize,prealloc,reuse,cached=$cached

define process name=filereader,instances=1
{
  thread name=filereaderthread,memsize=10m,instances=$nthreads
  {
    flowop read name=seqread_smallfile,filename=smallfile,iosize=$iosize,fd=3
    flowop read name=seqread_largefile,filename=largefile,iosize=$iosize,fd=4
  }
}

echo  "FileMicro-SeqRead-EachOne Version 1.0 personality successfully loaded"
usage "Usage: set \$dir=<dir>"
usage "       set \$cached=<bool>    defaults to $cached"
usage "       set \$filesize=<size>  defaults to $filesize"
usage "       set \$iosize=<size>    defaults to $iosize"
usage "       set \$nthreads=<value> defaults to $nthreads"
usage "       set \$small_filesize=<value> default to $small_filesize"
usage "       set \$large_filesize=<value> default to $large_filesize"
usage " "
usage "       run runtime (e.g. run 60)"

run 60